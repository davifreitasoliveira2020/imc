package com.mycompany.imc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author Davi
 */
public class LerArquivo {

    public static void main(String[] args){
        String path = "C:\\Users\\Davi\\Desktop\\dataset.csv";
        List<Imc> pessoas = new ArrayList<Imc>();
        BufferedReader lendo = null;
        String linha = "";
        try{
            lendo = new BufferedReader(new FileReader(path));
            linha = lendo.readLine();
            linha = lendo.readLine();
            FileWriter arq = new FileWriter("C:\\Users\\Davi\\Desktop\\[meuNomeCompleto].txt");
            PrintWriter gravarArq = new PrintWriter(arq);
            
            while(linha != null){
                    String[] vetor = linha.split(";", -1);
                    if(vetor[0].isEmpty() || vetor[1].isEmpty()){
                        linha = lendo.readLine();
                        continue;
                    } 
                    if(!(vetor[2].isEmpty() || vetor[3].isEmpty())){
                        String nome = vetor[0];
                        String sobrenome = vetor[1];
                        String numero = vetor[2];
                        numero = numero.replace(",",".");
                        float peso = Float.parseFloat(numero);
                        String numero2 = vetor[3];
                        numero2 = numero2.replace(",",".");
                        float altura = Float.parseFloat(numero2);
                        Imc pessoa = new Imc(nome, sobrenome, peso, altura);
                        pessoas.add(pessoa);
                        linha = lendo.readLine();
                        continue;
                    }  
                     else{
                        linha = lendo.readLine();
                        continue;
                    }
            }
            System.out.println("Pessoas:");
            for(Imc p: pessoas){
                gravarArq.printf("%s %s %s %n", p.getNome().toUpperCase().replaceAll("\\s+",""), p.getSobrenome().toUpperCase().replaceAll("\\s+"," "), p.calculaImc());
                System.out.println(p);     
            }
            arq.close();
          
        }
        catch (IOException e){
            System.out.println("Error ao ler arquivo:" + e.getMessage());          
        }
        finally {
            if(lendo != null) {
                try {
                    lendo.close();
                } catch(IOException e) {
                    System.out.println("Erro de entrada de dados: \n" + e.getMessage());
                }
            }
        }
    }
}       

