package com.mycompany.imc;


import java.text.DecimalFormat; 

public class Imc{
    private String nome, sobrenome, imc;
    private float peso, altura;
    

    public Imc(String nome, String sobrenome, float peso, float altura) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.peso = peso;
        this.altura = altura;
    }
    
    public String getNome() {
        return nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public float getPeso() {
        return peso;
    }

    public float getAltura() {
        return altura;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public String getImc() {
        return imc;
    }

    public void setImc(String imc) {
        this.imc = imc;
    }
    
    
    @Override
	public String toString() {
		return "name = " + nome.toUpperCase().replaceAll("\\s+","") + " " + sobrenome.toUpperCase().replaceAll("\\s+"," ") + "  " + calculaImc() + "";
	}
        
    public String calculaImc(){ 
        float calcImc = getPeso()/(getAltura()*getAltura());
        imc = formatarFloat(calcImc);
        return imc ;
    }
    
    public String formatarFloat(float numero){
        String retorno = "";
        DecimalFormat formatter = new DecimalFormat("#.00");
            try{
                 retorno = formatter.format(numero);
            }
            catch(Exception ex){
                System.err.println("Erro ao formatar numero: " + ex);
            }
            return retorno;
}

}


